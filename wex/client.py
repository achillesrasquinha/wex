import os, os.path as osp
import json

import requests

from   wex._util   import get_hmac_sha512
from   wex._compat import urlencode

import wex

class WEXClient:
    URL = 'https://wex.nz/tapi'

    def __init__(self, api_key, api_secret, nonce = 1):
        self.api_key    = api_key
        self.api_secret = api_secret
        self.nonce      = nonce

    def from_file(path):
        path = osp.abspath(path)

        if not osp.exists(path):
            raise wex.ValueError('{path} path does not exist.'.format(
                path = path
            ))
        else:
            with open(path, 'r') as f:
                data   = json.load(f)

                client = WEXClient(
                    api_key    = data['api_key'],
                    api_secret = data['api_secret'],
                    nonce      = data.get('nonce', 1)
                )

                return client

    def _get_signature(self, params):
        params      = urlencode(params)

        signature   = get_hmac_sha512(
            secret  = self.api_secret,
            payload = params
        )

        return signature

    def post(self, method, params = { }):
        params.update({
            "method": method,
             "nonce": self.nonce + 1
        })

        signature = self._get_signature(params)
        headers   = \
        {
            "Content-Type": "application/x-www-form-urlencoded",
                     "Key": self.api_key,
                    "Sign": signature
        }

        response  = requests.post(WEXClient.URL, headers = headers,
            data  = params
        )
        if response.ok:
            self.nonce += 1
            data        = response.json()
            if data['success']:
                return data['return']
            else:
                raise wex.WEXAPIError("Error for method {method}: {error}.".format(
                    method = method,
                    error  = data['error']
                ))
        else:
            response.raise_for_status()