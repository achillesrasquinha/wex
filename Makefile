-include .env

BASEDIR					= $(realpath .)
PROJECT					= wex

PROJDIR					= $(BASEDIR)/$(PROJECT)

install: clean
	pip install -r requirements.txt

	python setup.py develop

clean:
	find . | grep -E "__pycache__|\.pyc" | xargs rm -rf

	clear

console:
	ipython --no-banner

notebook: install
	jupyter notebook examples/Example.ipynb

test:
	pytest --cov=$(PROJDIR)