from setuptools import setup, find_packages

setup(
    name         = 'wex',
    version      = '0.1.0',
    url          = 'https://github.com/achillesrasquinha/wex.git',
    author       = 'Achilles Rasquinha',
    author_email = 'achillesrasquinha@gmail.com',
    description  = 'WEX Client Library',
    packages     = find_packages(exclude = ['test']),
)