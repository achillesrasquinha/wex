import sys

PY2 = sys.version_info[0] == 2

if PY2:
    from urlparse import urlencode
else:
    from urllib.parse import urlencode