import hmac
import hashlib

def safe_encode(objekt, encoding = 'utf-8'):
    try:
        objekt = objekt.encode(encoding)
    except Exception:
        pass
    return objekt

def get_hmac_sha512(secret, payload):
    secret     = safe_encode(secret)
    payload    = safe_encode(payload)

    h = hmac.new(secret, digestmod = hashlib.sha512)
    h.update(payload)

    signature  = h.hexdigest()

    return signature