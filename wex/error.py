class WEXError(Exception):
    pass

class ValueError(WEXError):
    pass

class WEXAPIError(WEXError):
    pass